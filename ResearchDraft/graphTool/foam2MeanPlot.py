import numpy as np
from scipy.interpolate import RegularGridInterpolator as rgi
import math
import pandas as pd

#Generate polar grid
rr = np.linspace(0,88,num=89)
thth = np.linspace(0,math.pi/2,100)

r = [np.full(len(thth),rr[i]) for i in range (len(rr))]
th = [thth for i in range(len(rr))]

y = [([]) for i in range(len(rr))]
z = [([]) for i in range(len(rr))]
for i in range(len(rr)):
    for j in range(len(thth)):
        y[i].append(r[i][j]*math.cos(th[i][j]))
        z[i].append(r[i][j]*math.sin(th[i][j]))

#Initialize directory path
import os
parent_path = os.getcwd()
path = parent_path + r'\ResearchDraft\dataGraph\reactingFoam\mean.csv'


with open(path,'r',encoding='utf-8') as f:
    text = f.readlines()

#List of desired parameter
par = ['H2Mean','O2Mean','N2Mean','H2OMean','TMean','pMean','UMean:0','UMean:1','UMean:2','Points:0','Points:1','Points:2']

#Convert data value into dictionary table
for i,line in list(enumerate(text)):
    val = line.strip().split(',')
    if i == 0:
        #Strip "" from keys string
        val = [keys.strip('"') for keys in val]
        #Take only desired parameter
        header = list(enumerate(val))
        for h,head in header:
            if head not in par:
                header.remove(h,head)
        #Initialize dictionary key
        tab = dict.fromkeys([header[i][1] for i in range(len(header))])
    elif i == 1:
        #Have to initialize list with different member
        for j,key in header:
            tab[key]=[val[j]]
    else:
        #Add the rest of the member
        for j,key in header:
            tab[key].append(val[j])

#Calculate interpolated value at prepared polar grid
Vhead = 'TMean'
V = tab(Vhead)
# X = tab('Points:0') #Normal to axial direction
Y = tab('Points:1')
Z = tab('Points:2')
interp = rgi((Y,Z), V)
pts = []
for i in range(rr):
    for j in range(thth):
        pts.append()

import matplotlib.pyplot as plt
        #Plot radial result for particular x/D position
        plt.plot(radius,m_T, '-ok')
        plt.xlabel('Radius (mm)')        
        plt.ylabel('Ensemble Mean Temperature (K)')
        plt.title('Radial Plot of Mean Temperature at x/D ={}'.format(val[i-1][0]))
        plt.legend()
        figureName='m_TxD={}.png'.format(val[i-1][0])
        plt.savefig(os.path.join(folder,figureName))
        plt.close()


        #Restart radius & value list
        radius = []
        m_T = []
        radius.append(val[i][1])
        m_T.append(val[i][3])