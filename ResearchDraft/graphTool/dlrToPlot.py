import matplotlib.pyplot as plt
import os

parent_path = os.getcwd()
path = parent_path + '\ResearchDraft\dataGraph\dlr_h2_a\M2\M2.AVE'
with open(path,'r',encoding='utf-8') as f:
    text = f.readlines()

#Convert data value into 2 dimensional list
val = []
for i,line in list(enumerate(text[9:-1])):
    val.append(line.strip().split())
    for j in range(len(val[i])):
        val[i][j] = float(val[i][j])

#Extract radius and mean Temperature value
radius=[]
m_T=[]
for i in range(len(val)):
    #If x/D same then add value to radial profile
    if val[i][0] == val[i-1][0] or val[i][0]==val[0][0]:
        radius.append(val[i][1])
        m_T.append(val[i][3])
    #else plot the graph and start at new x/D value
    else:
        folder=parent_path+'\\ResearchDraft\\dataGraph\\figureGraph'

        #Plot radial result for particular x/D position
        plt.plot(radius,m_T, '-ok')
        plt.xlabel('Radius (mm)')        
        plt.ylabel('Ensemble Mean Temperature (K)')
        plt.title('Radial Plot of Mean Temperature at x/D ={}'.format(val[i-1][0]))
        plt.legend()
        figureName='m_TxD={}.png'.format(val[i-1][0])
        plt.savefig(os.path.join(folder,figureName))
        plt.close()


        #Restart radius & value list
        radius = []
        m_T = []
        radius.append(val[i][1])
        m_T.append(val[i][3])